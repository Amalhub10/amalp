from django.contrib import admin
from django.urls import path
from .import views

urlpatterns = [
    #path('', views.index,name='index'),
    path('index11/', views.index11,name='index11'),
    path('index2/', views.index2,name='index2'),
   # path('v/', views.v,name='v'),
   # path('adminreg/', views.adminreg,name='adminreg'),
    path('admin1/', views.admin1,name='admin1'),
    path('admintable/', views.admintable,name='admintable'),
    path('adminregistration/', views.adminregistration,name='adminregistration'),
    path('updateadmin/<int:dataid>', views.updateadmin,name='updateadmin'),
    path('updating/<int:dataid>', views.updating,name='updating'),
    path('admindelete/<int:dataid>', views.admindelete,name='admindelete')
]
